//ТЕОРЕТИЧНІ ПИТАННЯ
// 1. Екранування - це спосіб використання спеціальних символів як звичайних
// 2. Засоби оголошення функцій:
// function example() {} - обʼявлення функції. Може викликатися до оголошення.
// let a = function() {} - функціональний вираз. Може викликатися тільки після оголошення.
// let a = () => {} - функціональний вираз.
// 3. Що таке hoisting, як він працює для змінних та функцій:
// Це процес підняття функції чи змінної в самий верх своєї області видимості. 
// Працює тільки з  var (ефект всплиття). У свою чергу звернутися до let чи const до оголошення не можна.
// Викликати функцію можна де завгодно, але тільки якщо вона оголошена.
 
//ПРАКТИЧНЕ ЗАВДАННЯ

function createNewUser(
    userFirstName = prompt("Enter your first name"),
    userLastName = prompt("Enter our last name"),
    userBirthday = prompt("Enter your birthday like dd.mm.yyyy")
  ) {
    this._firstName = userFirstName;
    this._lastName = userLastName;
    this._birthday = new Date(
      userBirthday.slice(6, 10),
      userBirthday.slice(3, 5) - 1,
      userBirthday.slice(0, 2)
    );
  
    this.getBirthday = function () {
      return this._birthday.toLocaleDateString();
    };
  
    this.getPassword = function () {
      return (
        this._firstName.charAt(0).toUpperCase() +
        this._lastName.toLowerCase() +
        this._birthday.getFullYear()
      );
    };
  
    this.getAge = function () {
      return (
        Math.floor(
          (new Date().getTime() - new Date(this._birthday)) /
            (24 * 3600 * 365.25 * 1000)
        ) + " years"
      );
    };
    this.getLogin = function () {
        return (this._firstName.charAt(0) + this._lastName).toLowerCase();
      };
    
      Object.defineProperty(this, "setFirstName", {
        set: function (value) {
          this._firstName = value;
        },
      }),
        Object.defineProperty(this, "setLastName", {
          set: function (value) {
            this._lastName = value;
          },
        });
    }
    
    let newUser = new createNewUser();
    console.log(newUser.getLogin());
    console.log("------------------------");

    console.log(newUser.getBirthday());
    console.log("------------------------");
    
    console.log(newUser.getAge());
    console.log("------------------------");
    
    console.log(newUser.getPassword());
    console.log("------------------------");
    